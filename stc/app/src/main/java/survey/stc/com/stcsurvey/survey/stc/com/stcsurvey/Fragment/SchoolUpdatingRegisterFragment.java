package survey.stc.com.stcsurvey.survey.stc.com.stcsurvey.Fragment;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import survey.stc.com.stcsurvey.R;
import survey.stc.com.stcsurvey.survey.stc.com.stcsurvey.pojo.CommonEnum;
import survey.stc.com.stcsurvey.survey.stc.com.stcsurvey.pojo.SchoolUpdatingData;
import survey.stc.com.stcsurvey.survey.stc.com.stcsurvey.pojo.SetupData;
import survey.stc.com.stcsurvey.survey.stc.com.stcsurvey.pojo.SurveyData;
import survey.stc.com.stcsurvey.survey.stc.com.stcsurvey.survey.stc.com.stcsurvey.util.CustomizeToast;

/**
 * Created by Htet Aung Naing on 10/18/2016.
 */

public class SchoolUpdatingRegisterFragment  extends Fragment implements AdapterView.OnItemSelectedListener{

    EditText txtSchoolCode;
    EditText txtTownship;
    EditText txtVillagename;
    Spinner activityType;
    EditText txtMaleEnrolled;
    EditText txtFemaleEnrolled;
    EditText txtMaleEnrolledPoor;
    EditText txtFemaleEnrolledPoor;
    EditText txtMaleEnrolledDisable;
    EditText txtFemaleEnrolledDisable;
    EditText txtMaleEnrollmentData;
    EditText txtFemaleEnrollmentData;
    EditText txtMaleEccdCgs;
    EditText txtFemaleEccdCgs;
    EditText txtMalePefs;
    EditText txtFemalePefs;
    EditText txtMaleEccdMcs;
    EditText txtFemaleEccdMcs;
    EditText txtDescEnrolled;
    EditText txtDescEnrolledPoor;
    EditText txtDescEnrolledDisable;
    EditText txtDescEnrollment;
    EditText txtDescEccdCgs;
    EditText txtDescEccdpfs;
    EditText txtDescEccdMcs;
    TextView lbltxtCode;
    TextView lblTowonship;
    TextView lblActivityType;
    TextView lblVillageName;
    TextView lblUpdatingDate;
    TextView lblEnrolledChildren;
    TextView lblEnrolledPoorChildren;
    TextView lblEnrolledDisableChildren;
    TextView lblEthnicEnrollment;
    TextView lblTrainedCGS;
    TextView lblTrainedPEFS;
    TextView lblTrainedMCS;

    Button butSave;
    View schoolUpdatingView;
    int activityKey = 0;
    List <String> activityList;
    Button btnUpdateDate;
    Date updatingDate;
    String username;
    String userid;
    int id = 0;
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

    public static SchoolUpdatingRegisterFragment newInstance(int id)
    {
        SchoolUpdatingRegisterFragment schoolRegisterFragment = new SchoolUpdatingRegisterFragment();
        Bundle args = new Bundle();
        args.putInt("id", id);
        schoolRegisterFragment.setArguments(args);

        return schoolRegisterFragment;
    }

    public void matchUi()
    {
        txtSchoolCode = (EditText) schoolUpdatingView.findViewById(R.id.txt_su_sccode);
        txtTownship = (EditText) schoolUpdatingView.findViewById(R.id.txt_su_Township);
        txtVillagename = (EditText) schoolUpdatingView.findViewById(R.id.txt_su_villageName);
        btnUpdateDate = (Button) schoolUpdatingView.findViewById(R.id.btn_su_date);
        activityType = (Spinner) schoolUpdatingView.findViewById(R.id.sp_su_sc_activity);
        txtMaleEnrolled = (EditText) schoolUpdatingView.findViewById(R.id.txt_male_enrolled_children_1);
        txtFemaleEnrolled = (EditText) schoolUpdatingView.findViewById(R.id.txt_female_enrolled_children_1);
        txtMaleEnrolledPoor = (EditText) schoolUpdatingView.findViewById(R.id.txt_male_enrolled_poor_2);
        txtFemaleEnrolledPoor = (EditText) schoolUpdatingView.findViewById(R.id.txt_female_enrolled_poor_2);
        txtMaleEnrolledDisable = (EditText) schoolUpdatingView.findViewById(R.id.txt_male_enrolled_disable_3);
        txtFemaleEnrolledDisable = (EditText) schoolUpdatingView.findViewById(R.id.txt_female_enrolled_disable_3);
        txtMaleEnrollmentData = (EditText) schoolUpdatingView.findViewById(R.id.txt_male_enrollment_4);
        txtFemaleEnrollmentData =  (EditText) schoolUpdatingView.findViewById(R.id.txt_female_enrollment_4);
        txtMaleEccdCgs = (EditText) schoolUpdatingView.findViewById(R.id.txt_male_eccds_cgs_5);
        txtFemaleEccdCgs = (EditText) schoolUpdatingView.findViewById(R.id.txt_female_eccds_cgs_5);
        txtMalePefs = (EditText) schoolUpdatingView.findViewById(R.id.txt_male_pefs_6);
        txtFemalePefs = (EditText) schoolUpdatingView.findViewById(R.id.txt_female_pefs_6);
        txtMaleEccdMcs = (EditText) schoolUpdatingView.findViewById(R.id.txt_male_eccds_mcs_7);
        txtFemaleEccdMcs = (EditText) schoolUpdatingView.findViewById(R.id.txt_female_eccds_mcs_7);
        butSave = (Button) schoolUpdatingView.findViewById(R.id.but_su_save);
        txtDescEnrolled = (EditText) schoolUpdatingView.findViewById(R.id.txt_description_enrolled_children_1);
        txtDescEnrolledPoor = (EditText) schoolUpdatingView.findViewById(R.id.txt_description_enrolled_poor_2);
        txtDescEnrolledDisable = (EditText) schoolUpdatingView.findViewById(R.id.txt_description_enrolled_disable_3);
        txtDescEnrollment = (EditText) schoolUpdatingView.findViewById(R.id.txt_description_enrollment_4);
        txtDescEccdCgs = (EditText) schoolUpdatingView.findViewById(R.id.txt_description_eccds_cgs_5);
        txtDescEccdpfs = (EditText) schoolUpdatingView.findViewById(R.id.txt_description_pefs_6);
        txtDescEccdMcs = (EditText) schoolUpdatingView.findViewById(R.id.txt_description_eccds_mcs_7);
        lbltxtCode = (TextView) schoolUpdatingView.findViewById(R.id.lblSchoolCode);
        lblTowonship = (TextView) schoolUpdatingView.findViewById(R.id.lblTownship);
        lblActivityType = (TextView) schoolUpdatingView.findViewById(R.id.lblActivity);
        lblVillageName = (TextView) schoolUpdatingView.findViewById(R.id.lbl_village);
        lblUpdatingDate = (TextView) schoolUpdatingView.findViewById(R.id.lblUpdatingDate);
        lblEnrolledChildren = (TextView) schoolUpdatingView.findViewById(R.id.lbl_su_enrolledChildren);
        lblEnrolledPoorChildren = (TextView) schoolUpdatingView.findViewById(R.id.lbl_su_enrolled_poor);
        lblEnrolledDisableChildren = (TextView) schoolUpdatingView.findViewById(R.id.lbl_su_enrolled_disable);
        lblEthnicEnrollment = (TextView) schoolUpdatingView.findViewById(R.id.lbl_su_enrollment_Ethnicdata);
        lblTrainedCGS = (TextView) schoolUpdatingView.findViewById(R.id.lbl_su_eccds_cgs);
        lblTrainedPEFS = (TextView) schoolUpdatingView.findViewById(R.id.lbl_su_pefs);
        lblTrainedMCS = (TextView) schoolUpdatingView.findViewById(R.id.lbl_su_eccds_mcs);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        this.id = getArguments().getInt("id");
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if(id == 0)
        {
            schoolUpdatingView  = inflater.inflate(R.layout.school_updating_register,container,false);

            Bundle bundle = this.getArguments();
            if(bundle != null)
            {
                userid = bundle.getString("userid");
                username = bundle.getString("username");
            }
            matchUi();
            createSpinner();
            butSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    if(isValidate(schoolUpdatingView.getContext()))
                    {
                        Realm.init(schoolUpdatingView.getContext());
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        SchoolUpdatingData school = realm.createObject(SchoolUpdatingData.class);
                        prepareData(school , realm);
                        realm.commitTransaction();
                        realm.close();
                        CustomizeToast cuToast = new CustomizeToast("info");
                        Toast toast = cuToast.getCustomizeToast(schoolUpdatingView.getContext(),"Save Successfully!");
                        toast.show();
                        school = new SchoolUpdatingData();
                        updateData(school);
                    }

                   /* Toast.makeText(schoolUpdatingView.getContext(),"Save Successfully!",Toast.LENGTH_LONG).show();
*/
                }
            });

            btnUpdateDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment newFragment = new DatePickerFragment(btnUpdateDate , updatingDate);
                    newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");

                }
            });
        }else
        {
            schoolUpdatingView  = inflater.inflate(R.layout.school_updating_register,container,false);
           final SchoolUpdatingData school = getSchoolUpdatingDataById(id , schoolUpdatingView.getContext());
            matchUi();
            createSpinner();
          updateData(school);
            butSave.setText("Update");

            butSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    if(isValidate(schoolUpdatingView.getContext()))
                    {
                        Realm.init(schoolUpdatingView.getContext());
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        SchoolUpdatingData updateSchool = realm.where(SchoolUpdatingData.class)
                                .equalTo("id", school.getId()).findFirst();
                        prepareData(updateSchool , realm);
                        realm.commitTransaction();
                        realm.close();
                        CustomizeToast cuToast = new CustomizeToast("info");
                        Toast toast = cuToast.getCustomizeToast(schoolUpdatingView.getContext(),"Update Successfully!");
                        toast.show();
                    }


                }
            });

            btnUpdateDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment newFragment = new DatePickerFragment(btnUpdateDate , updatingDate);
                    newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");

                }
            });
        }
        addTranslateEvent();

       return  schoolUpdatingView;
    }

    public void addTranslateEvent()
    {
        final Resources res = getResources();
        lbltxtCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.school_code_transl));
            }
        });

        lblTowonship.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.township_transl));
            }
        });

        lblActivityType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.activity_transl));
            }
        });

        lblVillageName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.village_transl));
            }
        });

        lblUpdatingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.updating_transl));
            }
        });

        lblEnrolledChildren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.ernolled_transl));
            }
        });

        lblEnrolledPoorChildren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.poor_transl));
            }
        });

        lblEnrolledDisableChildren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.disable_transl));
            }
        });

        lblEthnicEnrollment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.ethnic_transl));
            }
        });

        lblTrainedCGS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.trained_cgs_transl));
            }
        });

        lblTrainedPEFS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.trained_pefs_transl));
            }
        });

        lblTrainedMCS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMessage(res.getString(R.string.trained_mcs_transl));
            }
        });

    }

    public void showPopupMessage(String message)
    {
        CustomizeToast cuToast = new CustomizeToast("lbl");
        Toast toast = cuToast.getCustomizeToast(schoolUpdatingView.getContext(),message);
        toast.show();
    }

    public void updateData(SchoolUpdatingData school)
    {
        List<SurveyData> surveyList = school.getSchoolSurveyList();
        txtSchoolCode.setText(school.getSchoolCode());
        txtTownship.setText(school.getTowonship());
        txtVillagename.setText(school.getVillagName());
        activityType.setSelection(school.getActivityType());
        btnUpdateDate.setText(df.format(school.getMonitoringDate()));
        if(surveyList.size()>0)
        {
            txtMaleEnrolled.setText(String.valueOf(surveyList.get(0).getMalecount()));
            txtFemaleEnrolled.setText(String.valueOf(surveyList.get(0).getFemalecount()));
            txtMaleEnrolledPoor.setText(String.valueOf(surveyList.get(1).getMalecount()));
            txtFemaleEnrolledPoor.setText(String.valueOf(surveyList.get(1).getFemalecount()));
            txtMaleEnrolledDisable.setText(String.valueOf(surveyList.get(2).getMalecount()));
            txtFemaleEnrolledDisable.setText(String.valueOf(surveyList.get(2).getFemalecount()));
            txtMaleEnrollmentData.setText(String.valueOf(surveyList.get(3).getMalecount()));
            txtFemaleEnrollmentData.setText(String.valueOf(surveyList.get(3).getFemalecount()));
            txtMaleEccdCgs.setText(String.valueOf(surveyList.get(4).getMalecount()));
            txtFemaleEccdCgs.setText(String.valueOf(surveyList.get(4).getFemalecount()));
            txtMalePefs.setText(String.valueOf(surveyList.get(5).getMalecount()));
            txtFemalePefs.setText(String.valueOf(surveyList.get(5).getFemalecount()));
            txtMaleEccdMcs.setText(String.valueOf(surveyList.get(6).getMalecount()));
            txtFemaleEccdMcs.setText(String.valueOf(surveyList.get(6).getFemalecount()));
            txtDescEnrolled.setText(surveyList.get(0).getDescription());
            txtDescEnrolledPoor.setText(surveyList.get(1).getDescription());
            txtDescEnrolledDisable.setText(surveyList.get(2).getDescription());
            txtDescEnrollment.setText(surveyList.get(3).getDescription());
            txtDescEccdCgs.setText(surveyList.get(4).getDescription());
            txtDescEccdpfs.setText(surveyList.get(5).getDescription());
            txtDescEccdMcs.setText(surveyList.get(6).getDescription());
        }else
        {
            txtMaleEnrolled.setText(String.valueOf(0));
            txtFemaleEnrolled.setText(String.valueOf(0));
            txtMaleEnrolledPoor.setText(String.valueOf(0));
            txtFemaleEnrolledPoor.setText(String.valueOf(0));
            txtMaleEnrolledDisable.setText(String.valueOf(0));
            txtFemaleEnrolledDisable.setText(String.valueOf(0));
            txtMaleEnrollmentData.setText(String.valueOf(0));
            txtFemaleEnrollmentData.setText(String.valueOf(0));
            txtMaleEccdCgs.setText(String.valueOf(0));
            txtFemaleEccdCgs.setText(String.valueOf(0));
            txtMalePefs.setText(String.valueOf(0));
            txtFemalePefs.setText(String.valueOf(0));
            txtMaleEccdMcs.setText(String.valueOf(0));
            txtFemaleEccdMcs.setText(String.valueOf(0));
            txtDescEnrolled.setText("");
            txtDescEnrolledPoor.setText("");
            txtDescEnrolledDisable.setText("");
            txtDescEnrollment.setText("");
            txtDescEccdCgs.setText("");
            txtDescEccdpfs.setText("");
            txtDescEccdMcs.setText("");
        }

    }

    public SchoolUpdatingData getSchoolUpdatingDataById(int id,Context contex)
    {
        SchoolUpdatingData school = new SchoolUpdatingData();
        Realm.init(schoolUpdatingView.getContext());
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmResults<SchoolUpdatingData> results = realm.where(SchoolUpdatingData.class).equalTo("id",id).findAll();
        List<SchoolUpdatingData> schoolList = realm.copyFromRealm(results);
        school = schoolList.get(0);
        userid = school.getCreatedUsername();

        realm.commitTransaction();
        realm.close();
        return  school;
    }

    public void prepareData(SchoolUpdatingData school , Realm realm)
    {
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        school.setCreatedUsername(userid);
        school.setModifiedUsername(userid);
        school.setCreatedDate(df.parse(df.format(new Date())));
        school.setModifiedDate(df.parse(df.format(new Date())));
        school.setSchoolCode(txtSchoolCode.getText().toString());
        school.setTowonship(txtTownship.getText().toString());
        school.setActivityType(activityKey);
        school.setVillagName(txtVillagename.getText().toString());
        school.setMonitoringDate(df.parse(btnUpdateDate.getText().toString()));
        if(id == 0)
        {
            school.setId(getIdKey(realm));
            prepareSurveyData(school,realm);
        }else
        {
            prepareSurveyUpdateData(school,realm);
        }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int getIdKey(Realm realm)
    {
        int id = 0;
        id = realm.where(SchoolUpdatingData.class).max("id").intValue() + 1;
        return id;
    }

    public void prepareSurveyUpdateData(SchoolUpdatingData school,Realm realm)
    {
       setSurveyUpdateData(school.getSchoolSurveyList().get(0),txtMaleEnrolled.getText().toString(),txtFemaleEnrolled.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.enrollChildren.value(),CommonEnum.SchoolSurveyRequirement.enrollChildren.description(),txtDescEnrolled.getText().toString());
        setSurveyUpdateData(school.getSchoolSurveyList().get(1),txtMaleEnrolledPoor.getText().toString(),txtFemaleEnrolledPoor.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.poorChildEnrollment.value(),CommonEnum.SchoolSurveyRequirement.poorChildEnrollment.description(),txtDescEnrolledPoor.getText().toString());
        setSurveyUpdateData(school.getSchoolSurveyList().get(2),txtMaleEnrolledDisable.getText().toString(),txtFemaleEnrolledDisable.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.disableChildEnrollment.value(),CommonEnum.SchoolSurveyRequirement.disableChildEnrollment.description(),txtDescEnrolledDisable.getText().toString());
        setSurveyUpdateData(school.getSchoolSurveyList().get(3),txtMaleEnrollmentData.getText().toString(),txtFemaleEnrollmentData.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.enrollEthnic.value(),CommonEnum.SchoolSurveyRequirement.enrollEthnic.description(),txtDescEnrollment.getText().toString());
        setSurveyUpdateData(school.getSchoolSurveyList().get(4),txtMaleEccdCgs.getText().toString(),txtFemaleEccdCgs.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.trainEccd.value(),CommonEnum.SchoolSurveyRequirement.trainEccd.description(),txtDescEccdCgs.getText().toString());
        setSurveyUpdateData(school.getSchoolSurveyList().get(5),txtMalePefs.getText().toString(),txtFemalePefs.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.trainPefs.value(),CommonEnum.SchoolSurveyRequirement.trainPefs.description(),txtDescEccdpfs.getText().toString());
        setSurveyUpdateData(school.getSchoolSurveyList().get(6),txtMaleEccdMcs.getText().toString(),txtFemaleEccdMcs.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.trainedECCDMcs.value(),CommonEnum.SchoolSurveyRequirement.trainedECCDMcs.description(),txtDescEccdMcs.getText().toString());
    }

    public void prepareSurveyData(SchoolUpdatingData school,Realm realm)
    {
        school.getSchoolSurveyList().add(setSurveyData(realm,txtMaleEnrolled.getText().toString(),txtFemaleEnrolled.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.enrollChildren.value(),CommonEnum.SchoolSurveyRequirement.enrollChildren.description(),txtDescEnrolled.getText().toString()));
        school.getSchoolSurveyList().add(setSurveyData(realm,txtMaleEnrolledPoor.getText().toString(),txtFemaleEnrolledPoor.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.poorChildEnrollment.value(),CommonEnum.SchoolSurveyRequirement.poorChildEnrollment.description(),txtDescEnrolledPoor.getText().toString()));
        school.getSchoolSurveyList().add(setSurveyData(realm,txtMaleEnrolledDisable.getText().toString(),txtFemaleEnrolledDisable.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.disableChildEnrollment.value(),CommonEnum.SchoolSurveyRequirement.disableChildEnrollment.description(),txtDescEnrolledDisable.getText().toString()));
        school.getSchoolSurveyList().add(setSurveyData(realm,txtMaleEnrollmentData.getText().toString(),txtFemaleEnrollmentData.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.enrollEthnic.value(),CommonEnum.SchoolSurveyRequirement.enrollEthnic.description(),txtDescEnrollment.getText().toString()));
        school.getSchoolSurveyList().add(setSurveyData(realm,txtMaleEccdCgs.getText().toString(),txtFemaleEccdCgs.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.trainEccd.value(),CommonEnum.SchoolSurveyRequirement.trainEccd.description(),txtDescEccdCgs.getText().toString()));
        school.getSchoolSurveyList().add(setSurveyData(realm,txtMalePefs.getText().toString(),txtFemalePefs.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.trainPefs.value(),CommonEnum.SchoolSurveyRequirement.trainPefs.description(),txtDescEccdpfs.getText().toString()));
        school.getSchoolSurveyList().add(setSurveyData(realm,txtMaleEccdMcs.getText().toString(),txtFemaleEccdMcs.getText().toString(),
                CommonEnum.SchoolSurveyRequirement.trainedECCDMcs.value(),CommonEnum.SchoolSurveyRequirement.trainedECCDMcs.description(),txtDescEccdMcs.getText().toString()));
    }

    public void showCustomizeToast(String error,Context context)
    {
        CustomizeToast cuToast = new CustomizeToast("warn");
        Toast toast = cuToast.getCustomizeToast(context,error);
        toast.show();
    }

    public boolean isValidate(Context context)
    {
        boolean flag = true;
        if(txtSchoolCode.getText().toString().equals(""))
        {
            flag = false;
            showCustomizeToast("School Code must not be empty!" , context);
        }else if(txtTownship.getText().toString().equals(""))
        {
            flag = false;
            showCustomizeToast("Township must not be empty!",context);
        }else if(txtVillagename.getText().toString().equals(""))
        {
            flag = false;
            showCustomizeToast("Village name must not be empty!",context);
        }else if(activityKey == 0)
        {
            flag = false;
            showCustomizeToast("Select Type of Activity!",context);
        }else if(btnUpdateDate.getText().toString().equalsIgnoreCase("choose date"))
        {
            flag = false;
            showCustomizeToast("Choose Updating Date!",context);
        }

        return flag;
    }

    public SurveyData setSurveyData(Realm realm,String malecount, String femalecount, int typeid , String lbl , String description)
    {
        SurveyData survey = realm.createObject(SurveyData.class);
        survey.setCreatedUserName(userid);
        survey.setModifiedUserName(userid);
        survey.setCreatedDate(new Date());
        survey.setModifiedDate(new Date());
        if(!malecount.equals(""))
             survey.setMalecount(Integer.parseInt(malecount));
        else survey.setMalecount(0);
        if(!femalecount.equals(""))
             survey.setFemalecount(Integer.parseInt(femalecount));
        else survey.setFemalecount(0);
        survey.setTypeId(typeid);
        survey.setLabel(lbl);
        survey.setDescription(description);
        return survey;
    }

    public void setSurveyUpdateData(SurveyData survey,String malecount, String femalecount, int typeid , String lbl , String description)
    {
        survey.setCreatedUserName(userid);
        survey.setModifiedUserName(userid);
        survey.setCreatedDate(new Date());
        survey.setModifiedDate(new Date());
        if(!malecount.equals(""))
            survey.setMalecount(Integer.parseInt(malecount));
        else survey.setMalecount(0);
        if(!femalecount.equals(""))
            survey.setFemalecount(Integer.parseInt(femalecount));
        survey.setTypeId(typeid);
        survey.setLabel(lbl);
        survey.setDescription(description);
    }

    public void createSpinner()
    {
        activityList = getActivityData();
        activityType.setOnItemSelectedListener(this);
        ArrayAdapter<String> rarray = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, activityList);
        rarray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityType.setAdapter(rarray);
    }

    public List<String> getActivityData()
        {
        List<String>res = new ArrayList<String>() ;
        for(CommonEnum.ActivityStatus g : CommonEnum.ActivityStatus.values())
        {
            res.add(g.description());
        }

        return res;
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l)
    {
        activityKey = position;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


}
